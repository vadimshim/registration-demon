/**
 * Created by Vadim on 13.10.2016.
 */

const mongoose = require('mongoose');

const db = mongoose.createConnection('mongodb://localhost:27017/users');

module.exports = db;