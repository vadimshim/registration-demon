require('source-map-support').install();
import * as RPC from 'rpc-core';
import Server from './server';
import * as config from './config';

const service = new RPC.Claster(config.claster);
const server = new Server(config.server, service);
const db = require("./connection");

console.log("Server started on port:",config.server.port,", host:",config.server.host);