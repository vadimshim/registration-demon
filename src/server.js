import * as http from 'http';
import * as RPC from 'rpc-core';
import * as config from './config';

export default class Server {
    constructor(options, service) {
        this.options = options;
        this.service = service;
        this.init();
    }

    init() {
        this.native = http.createServer(this.onRequest.bind(this));
        this.native.listen(this.options.port, this.options.host);
    }

    async onRequest(request, response) {
        var requestTime = Date.now();

        try {
            var data = await getData(request);
            var client = await prepareClient(request);
            var result = await this.service.request(client, data);

            sendResponse(result.client, RPC.stringifyResponse(result.respose));
        }
        catch(error) {
            sendResponse(client, RPC.stringifyResponse({
                error: error
            }))
        }

        function sendResponse(client, data)
        {
            data = new Buffer(data);
            var now = Date.now();

            var headers = {
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': data.length,
                'Server-Time' : now,
                'Response-Time': now - requestTime
            };

            response.writeHead(200, headers);
            response.end(data);
        }
    }
}

function getData(request)
{
    return new Promise((done, fail) => {
        request.once('data', data => {
            try {
                data = RPC.parseRequest(data);
                done(data);
            }
            catch(error) {
                fail(error);
            }
        });
    });
}

function prepareClient(request)
{
    var client = {
        ip: request.headers['x-real-ip'],
        agent: request.headers['user-agent']
    };

    return client;
}
