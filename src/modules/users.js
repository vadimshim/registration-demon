import * as RPC from 'rpc-core';
const User = require("../schemas/users");

const utils = {
    trim(value) {
        if (value === null) {
            return null;
        }
        return value.trim()
    }
};


@RPC.module('users')
export default class Users {
    @RPC.attr('session', 'not')
    @RPC.params({
        name: {type: String, coerce: (value)=>
            {
                value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||@|\"|\+|\=|^(\b)/ig,()=>{return ""});
                value = value.replace(/\s+/g,()=>{return " "});
                value= utils.trim(value);
                return value;
            }},
        surname: {type: String, coerce: (value)=>
            {
                value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||@|\"|\+|\=|^(\b)/ig,()=>{return ""});
                value = value.replace(/\s+/g,()=>{return " "});
                value= utils.trim(value);
                return value;
            }},
        email: {type: String, coerce: (value)=>
            {
                value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||\"|\+|\=|^(\b)/ig,()=>{return ""});
                value = value.replace(/\s+/g,()=>{return " "});
                value= utils.trim(value);
                return value;
            }},
    })
    static async registration(params)
    {
        try{
            var user = await new User({name: params.name, surname: params.surname, email: params.email, checked: false});
            await user.save();
            var users = await User.find();
            return {code: -33000, message: "User successfully added"};
        }catch(ex){
            console.log(ex);
            if(ex.name == "MongoError" && ex.code == 11000){ // проверка на дубликаты полей name, surname, email
                var regName = new RegExp(params.name);
                var regSurname = new RegExp(params.surname);
                var regEmail = new RegExp(params.email);
                if(regName.test(ex.message)){
                    throw Errors.DublicateName(params.name);
                }else if(regSurname.test(ex.message)){
                    throw Errors.DublicateSurname(params.surname);
                }else if(regEmail.test(ex.message)){
                    throw Errors.DublicateEmail(params.email);
                }
            }
            if(ex.name == "ValidationError"){   //Проверка на пустой адрес почты
                throw Errors.EmptyEmail(ex.email);
            }
            throw Errors.UserCreateError(ex);
        }

    }
}


class Errors {
    static DublicateName(data) {
        return RPC.Errors.Error('Dublicate name', -31000, data);
    }

    static DublicateSurname(data) {
        return RPC.Errors.Error('Dublicate surname', -31001, data);
    }

    static DublicateEmail(data) {
        return RPC.Errors.Error('Dublicate email', -31002, data);
    }

    static UserCreateError(data) {
        return RPC.Errors.Error('User create error', -31099, data);
    }

    static EmptyEmail(data) {
        return RPC.Errors.Error('Email is required', -31010, data);
    }
}
