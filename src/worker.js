import * as sourceMap from 'source-map-support';
sourceMap.install();

import * as rpc from 'rpc-core';
import * as config from './config';

import Users from './modules/users';

global.worker = new rpc.Worker([
    Users
], {
    async init()
    {
        console.log('init request');
    },

    async end()
    {
        console.log('end request');
    },

    async call(method, params)
    {
        console.log('call method:', method.fullName);
    },

    async done(method, params, result)
    {
        console.log('done method:', method.fullName);
    },

    async fail(method, params, error)
    {
        console.log('fail method:', method.fullName);

        if( error instanceof Error ) {
            error.stack = sourceMap.getErrorSource(error);
            if( error.stack ) {
                error.stack = error.stack.substr(1)
            }

            console.log(error.message);
            console.log(error.stack);
        }
    }
});