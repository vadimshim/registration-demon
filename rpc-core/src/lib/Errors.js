export default class RPCError {
    constructor(message, code, data = undefined) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    message;
    code;
    data;
}

export function Error(message, code, data = undefined) {
    return new RPCError(message, code, data);
}

export function TransportError(data = undefined) {
    return new RPCError('Transport error.', -32300, data);
}

export function ParseError(data = undefined) {
    return new RPCError('Parse error.', -32700, data);
}

export function InvalidRequest(data = undefined) {
    return new RPCError('Invalid Request.', -32600, data);
}

export function InvalidResponse(data = undefined) {
    return new RPCError('Invalid Response.', -32600, data);
}

export function SystemError(data = undefined) {
    return new RPCError('System error.', -32400, data ? {
        message: data.message,
        stack: data.stack
    } : null);
}

export function MethodNotFound(method = undefined) {
    return new RPCError('Method not found.', -32601, method);
}

export function InvalidParams(param = undefined) {
    return new RPCError('Invalid Params.', -32602, param);
}

export function AccessDeniet(method = undefined) {
    return new RPCError('Access deniet.', -32504, method);
}