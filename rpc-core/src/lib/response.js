import * as Errors from './Errors';

export function parseResponse(response) {
    try {
        response = JSON.parse(response);
    }
    catch( error ) {
        throw Errors.ParseError(error);
    }

    if( !isValidResponse(response) ) {
        throw Errors.InvalidResponse();
    }

    return response;
}

export function stringifyResponse(id, result = undefined, error = undefined)
{
    if( id.constructor === Object ) {
        if( !id.jsonrpc ) {
            id.jsonrpc = "2.0";
        }
        return JSON.stringify(id);
    }

    return JSON.stringify({
        jsonrpc: "2.0",
        id: id,
        result: result,
        error: error
    });
}

export function isValidResponse(response)
{
    return response
        && response.constructor === Object
        && (
            response.result
            || (
                response.error
                && response.error.constructor === Object
                && response.error.message !== undefined
                && response.error.code !== undefined
                && response.error.code.constructor === Number
            )
        )
}