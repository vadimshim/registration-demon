import * as wc from 'worker-control';
import * as Errors from './Errors';

export default class Worker extends wc.Worker {
    constructor(define, handlers) {
        super();
        this.define = define;
        this.handlers = handlers || {};
        this.initDefine(define);
    }

    _defs = {};

    initDefine(define)
    {
        define.forEach(module => {
            var moduleName = module.key ? module.key + '.' : '';
            var propertes = Object.getOwnPropertyNames(module);

            propertes.forEach(methodName => {
                let method = module[methodName];
                if( method && method.constructor === Function ) {
                    method.handlers = this.handlers;
                    method.module = module;
                    method.fullName = moduleName + methodName;
                    this._defs[method.fullName] = method;
                }
            })
        })
    }

    getMethod(methodName, client, params) {
        var method = this._defs[methodName];
        if( !method || method.internal ) {
            throw Errors.MethodNotFound(methodName);
        }
        return method;
    }


    async onTask(task) {
        try {
            if( !task.client ) task.client = {};
            if( this.handlers.init ) {
                await this.handlers.init.call(task.client);
            }

            var method = this.getMethod(task.method, task.client, task.params);
	    var result = await method(task.client, task.params);
            if( this.handlers.end ) {
                try { await this.handlers.end.call(task.client); } catch(e) {
                    console.log('End Handler Error:', e);
                }
            }
            return result;
        }
        catch(error) {
            if( error instanceof Error ) {
                error = Errors.SystemError(error);
            }

            if( this.handlers.end ) {
                try { await this.handlers.end.call(task.client); } catch(e) {
                    console.log('End Handler Error:', e);
                }
            }
            throw error;
        }

    }
}