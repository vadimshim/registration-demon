import {cpus} from 'os';
import {parseRequest} from './request';
import WorkerControl from './WorkerControl';
import * as Errors from './Errors';

export default class Claster {
	constructor(config) {
		this.init(config);
	}
	
	workerFile;
	workersCount;
	workers;
	
	init(config)
	{
		process.on('exit', this.abort.bind(this));

		if( !config.workersCount || config.workersCount <= 0 ) {
			config.workersCount = cpus().length;
		}

		if( !config.workerFile ) {
			//todo: Error
		}

		this.workerFile = config.workerFile;
		this.workersCount = config.workersCount;
		this.workers = [];

		for(var i = 0; i < this.workersCount; i++ ) {
			this.workers[i] = new WorkerControl(this.workerFile);
		}
	}
	
	async request(client, request) {
		try {
			var result = await this.getWorker().sendTask({
				client: client,
				method: request.method,
				params: request.params
			});
			return {
				client: result.client,
				respose: {
					jsonrpc: "2.0",
					id: request.id,
					result: result || undefined,
					error: result.error || undefined
				}
			}
		}
		catch( error ) {
			if( error instanceof Error ) {
                error = Errors.SystemError(error);
            }
			
			return {
				client: client,
				respose: {
					jsonrpc: "2.0",
					id: request.id !== undefined ? request.id : (error.code === -32600 ? error.data : undefined),
					error: error
				}
			}
		}
	}
	
	restart(count = 0)
	{
		this.abort();
		this.workers = [];
		
		if( count !== 0 ) {
			this.workersCount = count;
		}
				
		for(let i = 0; i < this.workersCount; i++) {
			this.workers[i] = new WorkerControl(this.workerFile);
		}
	}

	abort()
	{
		for(let i = 0; i < this.workersCount; i++) {
			this.workers[i].destroy();
		}
	}
	
	getWorker()
    {
		/*if( !worker ) {
			throw Error.SystemError('Not ready workers');
		}*/
        var minWorker = this.workers[0];
        for(var i = 1; i < this.workersCount; i++) {
            var worker = this.workers[i];
            if( worker.count > minWorker.count ) continue;
            minWorker = worker;
        }
        return minWorker;
    }
}

