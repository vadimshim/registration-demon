import * as Error from './Errors';

export function createMethod(descriptor, name) {
    if( descriptor.value.original ) return descriptor.value;

    var method = async function(client, params) {
        var preparedParams = {};

        if( !params ) {
            params = {};
        }

        for (var i = 0, count = method.params.length; i < count; i++) {
            var defparam = method.params[i];
            preparedParams[defparam.name] = defparam.prepare(params[defparam.name]);
        }

        try {
            if( method.handlers.call ) {
                await method.handlers.call.call(client, method, preparedParams);
            }

            var result = await method.original.call(client, preparedParams);

            try {
                if( method.handlers.done ) {
                    await method.handlers.done.call(client, method, preparedParams, result);
                }
            }
            catch(e) {
                console.log(`\n"Done" operation failed on "${method.key}" method`);
                console.log(e.stack);
            }
            return result;
        }
        catch(error) {
            try {
                if( method.handlers.fail ) {
                    await method.handlers.fail.call(client, method, preparedParams, error);
                }
            }
            catch(e) {
                console.log(`\n"Fail" operation failed on "${method.key}" method`);
                console.log(e.stack);
            }

            throw error;
        }

        /*
         if( method.events ) {
             for(var i = 0, count = method.events.length; i < count; i++) {
             try {
                method.events[i].delegate.call(this, error, result);
             }
             catch( e ) {
                 console.log('\nEvent "' + method.events[i].method + '" failed');
                 console.log(e.stack);
             }
             }
         }
         */
    };

    method.original = descriptor.value;
    method.key = name;
    method.attr = {};
    method.triggers = [];
    method.params = [];

    return descriptor.value = method;
}