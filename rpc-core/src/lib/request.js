import * as Errors from './Errors';

export function parseRequest(request) {
    try {
        request = JSON.parse(request);
    }
    catch( error ) {
        throw Errors.ParseError(error);
    }

    if( !isValidRequest(request) ) {
        throw Errors.InvalidRequest(request.id);
    }

    return request;
}

export function stringifyRequest(id, method = undefined, params = undefined)
{
    if( id.constructor === Object ) {
        if( !id.jsonrpc ) {
            id.jsonrpc = "2.0";
        }
        return JSON.stringify(id);
    }

    return JSON.stringify({
        jsonrpc: "2.0",
        id: id,
        method: method,
        params: params
    });
}

function isValidRequest(request)
{
    return request
        && request.constructor === Object
        && (
            request.method
            && request.method.constructor === String
            && /^\w+(\.\w+)*$/.test(request.method)
        )
        && (
            !request.params
            || (
                request.params
                && (
                    request.params.constructor === Object
                    || request.params.constructor === Array
                )
            )
        )
}

