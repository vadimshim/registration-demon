import * as Error from './Errors';
import {Param} from './Param';
import {createMethod} from './Method';


export function internal(target, key, descriptor) {
    var method = createMethod(descriptor, key);
    method.internal = true;    
}

export function module(name) {
    return function(target) {
        target.key = name;
    }
}

export function attr(attr, value) {
    return function(target, key, descriptor) {
        var method = createMethod(descriptor, key);

        switch( attr.constructor ) {
            case Object:
                for( var key in attr ) {
                    if( !attr.hasOwnProperty(key) ) continue;
                    method.attr[key] = attr[key];
                }
                break;
                
            case String:
                if( value !== undefined ) {
                    method.attr[attr] = value;
                }
                else {
                    method.attr[attr] = true;
                }
                break;
        }
    }
}

export function trigger(targetMethod, handler) {
    return function(target, key, descriptor) {
        var method = createMethod(descriptor, key);

        method.triggers.push({targetMethod, handler});
    }
}

export function params(params) {
    return function(target, key, descriptor) {
        var method = createMethod(descriptor, key);
                
        for (var key in params) {
            if (!params.hasOwnProperty(key)) continue;
            method.params.push(Param.create(key, params[key]));
        }
    }
}


