import {EventEmitter} from 'events';
import * as Errors from './Errors';

export default class InstanceBase extends EventEmitter {
    constructor(define, handlers) {
        super();
        this.handlers = handlers || {};
        this.initDefine(define);
    }

    handlers;
    _defs = {};
    initDefine(define)
    {
        define.forEach(module => {
            var moduleName = module.key ? module.key + '.' : '';
            var propertes = Object.getOwnPropertyNames(module);

            propertes.forEach(methodName => {
                let method = module[methodName];
                if( method && method.constructor === Function ) {
                    method.handlers = this.handlers;
                    method.module = module;
                    method.fullName = moduleName + methodName;
                    this._defs[method.fullName] = method;
                }
            })
        })
    }

    getMethod(methodName) {
        var method = this._defs[methodName];
        if( !method || method.internal ) {
            throw Errors.MethodNotFound(methodName);
        }
        return method;
    }
}