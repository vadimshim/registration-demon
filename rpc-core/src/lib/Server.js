import InstanceBase from './InstanceBase';
import * as Errors from './Errors';

export default class Server extends InstanceBase {
    constructor(define, handlers) {
        super(define, handlers);
    }

    async request(client, request) {
        if( !client ) client = {};
        try {
            if( this.handlers.init ) {
                await this.handlers.init.call(client);
            }

            var method = this.getMethod(request.method);
            var result = await method(client, request.params);

            if( this.handlers.end ) {
                try { await this.handlers.end.call(client); } catch(e) {
                    console.log('End Handler Error:', e);
                }
            }

            return {
                client: client,
                respose: {
                    jsonrpc: "2.0",
                    id: request.id,
                    result: result
                }
            }
        }
        catch( error ) {
            if( error instanceof Error ) {
                error = Errors.SystemError(error);
            }

            if( this.handlers.end ) {
                try { await this.handlers.end.call(client); } catch(e) {
                    console.log('End Handler Error:', e);
                }
            }

            return {
                client: client,
                respose: {
                    jsonrpc: "2.0",
                    id: request.id !== undefined ? request.id : (error.code === -32600 ? error.data : undefined),
                    error: error
                }
            }
        }
    }
}