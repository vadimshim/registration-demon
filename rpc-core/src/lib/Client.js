import * as Errors from './Errors';
//import {parseResponse} from './response';

export default class Client {
    constructor(options, transport)
    {
        if( transport === undefined ) {
            transport = options;
            options = {};
        }
        this.initOptions(options);
        this.transport = transport;
        this.GUID = 0;
        this.isConnected = false;
        this.awaitRequests = [];

        this.connect = transport.connect;
        this.send = transport.send;
        this.destroy = transport.destroy;

        this.connect();

        this._initTimer();
    }

    initOptions(options)
    {
        if( options.requestTimeout === undefined ) {
            options.requestTimeout = 3 * 60 * 1000;
        }

        this.options = options;
    }

    _initTimer()
    {
        setTimeout(() =>
        {
            var expire = Date.now() - this.options.requestTimeout;

            for(var i = 0, count = this.awaitRequests.length; i < count; i++) {
                var request = this.awaitRequests[i];
                if( request.time <= expire ) {
                    request.fail(Errors.SystemError());

                    this.awaitRequests.splice(i, 1);
                    i--;
                    count--;
                }
            }

            this._initTimer();
        }, this.options.requestTimeout / 2);
    }

    call(method, params)
    {
        if( !this.isConnected ) {
           throw Errors.SystemError();
        }

        return new Promise((done, fail) =>
        {
            var request = {
                id: this.generateRequId(),
                done: done,
                fail: fail,
                expire: Date.now()
            };

            this.awaitRequests.push(request);

            this.send(JSON.stringify({
                "jsonrpc": "2.0",
                id: request.id,
                method: method,
                params: params
            }), request.id);
        });
    }

    onData(data)
    {
        try {
            data = JSON.parse(data);
        }
        catch (e) {
            return; //todo: Ошибка, придумать что делать
        }

        for(var i = 0, count = this.awaitRequests.length; i < count; i++) {
            var request = this.awaitRequests[i];
            if( request.id === data.id ) {
                if( data.error ) {
                    request.fail(data.error);
                }
                else {
                    request.done(data.result || null);
                }
                this.awaitRequests.splice(i, 1);
                break;
            }
        }
    }

    onConnected()
    {
        this.isConnected = true;
    }

    onDisconnected()
    {
        this.isConnected = false;

        this.awaitRequests.forEach(request=> {
            request.fail(Errors.SystemError());
        });

        this.awaitRequests = [];
        this.destroy();

        setTimeout(this.connect.bind(this), 100);
    }

    generateRequId()
    {
        var id = this.GUID++;

        if( this.GUID >= Number.MAX_VALUE ) {
            this.GUID = 0;
        }

        return id;
    }
}