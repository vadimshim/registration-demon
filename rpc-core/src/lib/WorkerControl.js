import * as wc from 'worker-control';
import * as Errors from './Errors';


export default class WorkerControl extends wc.WorkerControler {
	constructor(file) {
		super(file);
	}

	workerError(e) { // Перезаписали данный метод для использования Errors
        this.tasksArray.forEach(value =>{
            var task = value;
        if( task ) {
            task.fail(Errors.SystemError(e || "Process Crushed"));
        }
    })
        this.tasksArray.clear();
        this.count = 0;

        this.killChild();

        for(var i = 0; i < 20; i++){
            if(!this.isReady && this.isDestroy){
                if( this.isReady && !this.isDestroy ) {
                    this.init();
                }
                else {
                    console.log("The process_child cannot be forked");
                }
            }
        }
	}
}