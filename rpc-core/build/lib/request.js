var _interopRequireWildcard = require("babel-runtime/helpers/interop-require-wildcard")["default"];

exports.__esModule = true;
exports.parseRequest = parseRequest;
exports.stringifyRequest = stringifyRequest;

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

function parseRequest(request) {
    try {
        request = JSON.parse(request);
    } catch (error) {
        throw Errors.ParseError(error);
    }

    if (!isValidRequest(request)) {
        throw Errors.InvalidRequest(request.id);
    }

    return request;
}

function stringifyRequest(id) {
    var method = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];
    var params = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

    if (id.constructor === Object) {
        if (!id.jsonrpc) {
            id.jsonrpc = "2.0";
        }
        return JSON.stringify(id);
    }

    return JSON.stringify({
        jsonrpc: "2.0",
        id: id,
        method: method,
        params: params
    });
}

function isValidRequest(request) {
    return request && request.constructor === Object && request.method && request.method.constructor === String && /^\w+(\.\w+)*$/.test(request.method) && (!request.params || request.params && (request.params.constructor === Object || request.params.constructor === Array));
}
//# sourceMappingURL=request.js.map
