var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _Object$getOwnPropertyNames = require('babel-runtime/core-js/object/get-own-property-names')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _events = require('events');

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

var InstanceBase = (function (_EventEmitter) {
    _inherits(InstanceBase, _EventEmitter);

    function InstanceBase(define, handlers) {
        _classCallCheck(this, InstanceBase);

        _EventEmitter.call(this);
        this._defs = {};
        this.handlers = handlers || {};
        this.initDefine(define);
    }

    InstanceBase.prototype.initDefine = function initDefine(define) {
        var _this = this;

        define.forEach(function (module) {
            var moduleName = module.key ? module.key + '.' : '';
            var propertes = _Object$getOwnPropertyNames(module);

            propertes.forEach(function (methodName) {
                var method = module[methodName];
                if (method && method.constructor === Function) {
                    method.handlers = _this.handlers;
                    method.module = module;
                    method.fullName = moduleName + methodName;
                    _this._defs[method.fullName] = method;
                }
            });
        });
    };

    InstanceBase.prototype.getMethod = function getMethod(methodName) {
        var method = this._defs[methodName];
        if (!method || method.internal) {
            throw Errors.MethodNotFound(methodName);
        }
        return method;
    };

    return InstanceBase;
})(_events.EventEmitter);

exports['default'] = InstanceBase;
module.exports = exports['default'];
//# sourceMappingURL=InstanceBase.js.map
