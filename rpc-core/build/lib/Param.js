var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _Errors = require('./Errors');

var Error = _interopRequireWildcard(_Errors);

var Param = (function () {
    function Param(name, declaration) {
        _classCallCheck(this, Param);

        this.name = name;
        if (!declaration) return;

        if (declaration.type !== undefined) {
            this.type = declaration.type;
        }

        if (declaration.hasOwnProperty('default')) {
            this['default'] = declaration['default'];
        }

        if (declaration.coerce !== undefined) {
            this.coerce = declaration.coerce;
        }
        if (declaration.validate !== undefined) {
            switch (declaration.validate.constructor) {
                case Function:
                    this.validate = declaration.validate;
                    break;

                case RegExp:
                    this.validate = Param.createValidationFromRegExp(declaration.validate);
                    break;

                case Array:
                    this.validate = Param.createValidationFromArray(declaration.validate);
                    break;

                default:
                    throw new Error('Param "' + name + '" validate error');
                    break;
            }
        }

        if (declaration.convert !== undefined) {
            this.convert = declaration.convert;
        }
    }

    Param.prototype.prepare = function prepare(value) {
        if (value === undefined) {
            if (this.hasOwnProperty('default')) {
                if (this['default'] && this['default'].constructor === Function) {
                    return this['default']();
                } else {
                    return this['default'];
                }
            }

            throw Error.InvalidParams(this.name);
        }

        if (this.type && value !== null) {
            if (value.constructor !== this.type) {
                throw Error.InvalidParams(value);
            }
        }

        if (this.coerce) {
            value = this.coerce(value);
        }

        if (this.validate && !this.validate(value)) {
            throw Error.InvalidParams(this.name);
        }

        if (this.convert) {
            value = this.convert(value);
        }

        return value;
    };

    Param.createValidationFromRegExp = function createValidationFromRegExp(reg) {
        return function (value) {
            return reg.test(value);
        };
    };

    Param.createValidationFromArray = function createValidationFromArray(arr) {
        return function (value) {
            return arr.indexOf(value) >= 0;
        };
    };

    Param.create = function create(name, proto) {
        if (!proto.type || proto.type.constructor === Function) {
            if (proto.type === Array) {
                //для случая, когда параметром функции является массив
                return new arrayParam(name, proto);
            } else {
                return new Param(name, proto);
            }
        } else switch (proto.type.constructor) {
            case Object:
                return new ObjectParam(name, proto);
            case Array:
                return new VariantsParam(name, proto);
        }
    };

    return Param;
})();

exports.Param = Param;

var arrayParam = (function (_Param) {
    _inherits(arrayParam, _Param);

    function arrayParam(name, declaration) {
        _classCallCheck(this, arrayParam);

        _Param.call(this, name, declaration);
        this.name = name;

        if (!declaration) return;

        if (declaration.hasOwnProperty('default')) {
            this['default'] = declaration['default'];
        }

        if (declaration.type !== undefined) {
            this.type = declaration.type;
        }

        if (declaration.min !== undefined) {
            this.min = declaration.min;
        }

        if (declaration.max !== undefined) {
            this.max = declaration.max;
        }

        if (declaration.coerce !== undefined) {
            this.coerce = declaration.coerce;
        }

        if (declaration.elements !== undefined) {
            this.elements = new Param.create("elements", declaration.elements);
        }

        if (declaration.validate !== undefined) {
            switch (declaration.validate.constructor) {
                case Function:
                    this.validate = declaration.validate;
                    break;

                case RegExp:
                    this.validate = Param.createValidationFromRegExp(declaration.validate);
                    break;

                case Array:
                    this.validate = Param.createValidationFromArray(declaration.validate);
                    break;

                default:
                    throw new Error('Param "' + name + '" validate error');
                    break;
            }
        }
    }

    arrayParam.prototype.prepare = function prepare(value) {
        if (value === undefined) {
            if (this.hasOwnProperty('default')) {
                if (this['default'] && this['default'].constructor === Function) {
                    return this['default']();
                } else {
                    return this['default'];
                }
            }

            throw Error.InvalidParams(this.name);
        }

        if (value !== null) {
            if (value.constructor !== this.type) {
                throw Error.InvalidParams(value);
            }
        }

        if (this.coerce) {
            value = this.coerce(value);
        }

        if (this.min) {
            if (value.length < this.min) {
                throw Error.InvalidParams(value);
            }
        }

        if (this.max) {
            if (value.length > this.max) {
                throw Error.InvalidParams(value);
            }
        }

        if (this.validate && !this.validate(value)) {
            throw Error.InvalidParams(value);
        }

        if (this.convert) {
            value = this.convert(value);
        }

        if (this.elements) {
            var preparedElements = [];
            for (var i = 0; i < value.length; i++) {
                preparedElements.push(this.elements.prepare(value[i]));
            }
            return preparedElements;
        }

        return value;
    };

    return arrayParam;
})(Param);

var ObjectParam = (function (_Param2) {
    _inherits(ObjectParam, _Param2);

    function ObjectParam(name, proto) {
        _classCallCheck(this, ObjectParam);

        _Param2.call(this, name);

        if (proto.hasOwnProperty('default')) {
            this['default'] = proto['default'];
        }

        this.type = Object;
        this.fields = [];

        for (var key in proto.type) {
            if (!proto.type.hasOwnProperty(key)) continue;

            this.fields.push(Param.create(key, proto.type[key]));
        }
    }

    ObjectParam.prototype.prepare = function prepare(value) {
        if (value === undefined) {
            if (this.hasOwnProperty('default')) {
                if (this['default'] && this['default'].constructor === Function) {
                    return this['default']();
                } else {
                    return this['default'];
                }
            }

            throw Error.InvalidParams(this.name);
        }

        if (value !== null) {
            if (value.constructor !== this.type) {
                throw Error.InvalidParams(this.name);
            }

            var preparedParams = {};

            try {
                for (var i = 0, count = this.fields.length; i < count; i++) {
                    var defparam = this.fields[i];
                    preparedParams[defparam.name] = defparam.prepare(value[defparam.name]);
                }
            } catch (error) {
                if (error.code === -32602) {
                    error.data = this.name + '.' + error.data;
                }

                throw error;
            }
        }

        return preparedParams;
    };

    return ObjectParam;
})(Param);

var VariantsParam = (function (_Param3) {
    _inherits(VariantsParam, _Param3);

    function VariantsParam(name, proto) {
        _classCallCheck(this, VariantsParam);

        _Param3.call(this, name);

        if (proto.hasOwnProperty('default')) {
            this['default'] = proto['default'];
        }

        this.types = proto.type.map(function (type) {
            return Param.create(name, type);
        });
    }

    VariantsParam.prototype.prepare = function prepare(value) {
        if (value === undefined) {
            if (this.hasOwnProperty('default')) {
                if (this['default'] && this['default'].constructor === Function) {
                    return this['default']();
                } else {
                    return this['default'];
                }
            }

            throw Error.InvalidParams(this.name);
        }

        if (value !== null) {
            var type = null;
            for (var i = 0, count = this.types.length; i < count; i++) {

                //todo: учесть вариационный тип
                if (value.constructor === this.types[i].type) {
                    type = this.types[i];
                    break;
                }
            }

            if (!type) {
                throw Error.InvalidParams(this.name);
            }

            return type.prepare(value);
        }

        return value;
    };

    return VariantsParam;
})(Param);
//# sourceMappingURL=Param.js.map
