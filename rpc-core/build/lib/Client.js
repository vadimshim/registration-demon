var _classCallCheck = require("babel-runtime/helpers/class-call-check")["default"];

var _Promise = require("babel-runtime/core-js/promise")["default"];

var _interopRequireWildcard = require("babel-runtime/helpers/interop-require-wildcard")["default"];

exports.__esModule = true;

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

//import {parseResponse} from './response';

var Client = (function () {
    function Client(options, transport) {
        _classCallCheck(this, Client);

        if (transport === undefined) {
            transport = options;
            options = {};
        }
        this.initOptions(options);
        this.transport = transport;
        this.GUID = 0;
        this.isConnected = false;
        this.awaitRequests = [];

        this.connect = transport.connect;
        this.send = transport.send;
        this.destroy = transport.destroy;

        this.connect();

        this._initTimer();
    }

    Client.prototype.initOptions = function initOptions(options) {
        if (options.requestTimeout === undefined) {
            options.requestTimeout = 3 * 60 * 1000;
        }

        this.options = options;
    };

    Client.prototype._initTimer = function _initTimer() {
        var _this = this;

        setTimeout(function () {
            var expire = Date.now() - _this.options.requestTimeout;

            for (var i = 0, count = _this.awaitRequests.length; i < count; i++) {
                var request = _this.awaitRequests[i];
                if (request.time <= expire) {
                    request.fail(Errors.SystemError());

                    _this.awaitRequests.splice(i, 1);
                    i--;
                    count--;
                }
            }

            _this._initTimer();
        }, this.options.requestTimeout / 2);
    };

    Client.prototype.call = function call(method, params) {
        var _this2 = this;

        if (!this.isConnected) {
            throw Errors.SystemError();
        }

        return new _Promise(function (done, fail) {
            var request = {
                id: _this2.generateRequId(),
                done: done,
                fail: fail,
                expire: Date.now()
            };

            _this2.awaitRequests.push(request);

            _this2.send(JSON.stringify({
                "jsonrpc": "2.0",
                id: request.id,
                method: method,
                params: params
            }), request.id);
        });
    };

    Client.prototype.onData = function onData(data) {
        try {
            data = JSON.parse(data);
        } catch (e) {
            return; //todo: Ошибка, придумать что делать
        }

        for (var i = 0, count = this.awaitRequests.length; i < count; i++) {
            var request = this.awaitRequests[i];
            if (request.id === data.id) {
                if (data.error) {
                    request.fail(data.error);
                } else {
                    request.done(data.result || null);
                }
                this.awaitRequests.splice(i, 1);
                break;
            }
        }
    };

    Client.prototype.onConnected = function onConnected() {
        this.isConnected = true;
    };

    Client.prototype.onDisconnected = function onDisconnected() {
        this.isConnected = false;

        this.awaitRequests.forEach(function (request) {
            request.fail(Errors.SystemError());
        });

        this.awaitRequests = [];
        this.destroy();

        setTimeout(this.connect.bind(this), 100);
    };

    Client.prototype.generateRequId = function generateRequId() {
        var id = this.GUID++;

        if (this.GUID >= Number.MAX_VALUE) {
            this.GUID = 0;
        }

        return id;
    };

    return Client;
})();

exports["default"] = Client;
module.exports = exports["default"];
//# sourceMappingURL=Client.js.map
