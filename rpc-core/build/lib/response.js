var _interopRequireWildcard = require("babel-runtime/helpers/interop-require-wildcard")["default"];

exports.__esModule = true;
exports.parseResponse = parseResponse;
exports.stringifyResponse = stringifyResponse;
exports.isValidResponse = isValidResponse;

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

function parseResponse(response) {
    try {
        response = JSON.parse(response);
    } catch (error) {
        throw Errors.ParseError(error);
    }

    if (!isValidResponse(response)) {
        throw Errors.InvalidResponse();
    }

    return response;
}

function stringifyResponse(id) {
    var result = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];
    var error = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

    if (id.constructor === Object) {
        if (!id.jsonrpc) {
            id.jsonrpc = "2.0";
        }
        return JSON.stringify(id);
    }

    return JSON.stringify({
        jsonrpc: "2.0",
        id: id,
        result: result,
        error: error
    });
}

function isValidResponse(response) {
    return response && response.constructor === Object && (response.result || response.error && response.error.constructor === Object && response.error.message !== undefined && response.error.code !== undefined && response.error.code.constructor === Number);
}
//# sourceMappingURL=response.js.map
