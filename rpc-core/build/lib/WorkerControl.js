var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _workerControl = require('worker-control');

var wc = _interopRequireWildcard(_workerControl);

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

var WorkerControl = (function (_wc$WorkerControler) {
    _inherits(WorkerControl, _wc$WorkerControler);

    function WorkerControl(file) {
        _classCallCheck(this, WorkerControl);

        _wc$WorkerControler.call(this, file);
    }

    WorkerControl.prototype.workerError = function workerError(e) {
        // Перезаписали данный метод для использования Errors
        this.tasksArray.forEach(function (value) {
            var task = value;
            if (task) {
                task.fail(Errors.SystemError(e || "Process Crushed"));
            }
        });
        this.tasksArray.clear();
        this.count = 0;

        this.killChild();

        for (var i = 0; i < 20; i++) {
            if (!this.isReady && this.isDestroy) {
                if (this.isReady && !this.isDestroy) {
                    this.init();
                } else {
                    console.log("The process_child cannot be forked");
                }
            }
        }
    };

    return WorkerControl;
})(wc.WorkerControler);

exports['default'] = WorkerControl;
module.exports = exports['default'];
//# sourceMappingURL=WorkerControl.js.map
