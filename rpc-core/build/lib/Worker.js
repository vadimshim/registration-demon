var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _Object$getOwnPropertyNames = require('babel-runtime/core-js/object/get-own-property-names')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _workerControl = require('worker-control');

var wc = _interopRequireWildcard(_workerControl);

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

var Worker = (function (_wc$Worker) {
    _inherits(Worker, _wc$Worker);

    function Worker(define, handlers) {
        _classCallCheck(this, Worker);

        _wc$Worker.call(this);
        this._defs = {};
        this.define = define;
        this.handlers = handlers || {};
        this.initDefine(define);
    }

    Worker.prototype.initDefine = function initDefine(define) {
        var _this = this;

        define.forEach(function (module) {
            var moduleName = module.key ? module.key + '.' : '';
            var propertes = _Object$getOwnPropertyNames(module);

            propertes.forEach(function (methodName) {
                var method = module[methodName];
                if (method && method.constructor === Function) {
                    method.handlers = _this.handlers;
                    method.module = module;
                    method.fullName = moduleName + methodName;
                    _this._defs[method.fullName] = method;
                }
            });
        });
    };

    Worker.prototype.getMethod = function getMethod(methodName, client, params) {
        var method = this._defs[methodName];
        if (!method || method.internal) {
            throw Errors.MethodNotFound(methodName);
        }
        return method;
    };

    Worker.prototype.onTask = _asyncToGenerator(function* (task) {
        try {
            if (!task.client) task.client = {};
            if (this.handlers.init) {
                yield this.handlers.init.call(task.client);
            }

            var method = this.getMethod(task.method, task.client, task.params);
            var result = yield method(task.client, task.params);
            if (this.handlers.end) {
                try {
                    yield this.handlers.end.call(task.client);
                } catch (e) {
                    console.log('End Handler Error:', e);
                }
            }
            return result;
        } catch (error) {
            if (error instanceof Error) {
                error = Errors.SystemError(error);
            }

            if (this.handlers.end) {
                try {
                    yield this.handlers.end.call(task.client);
                } catch (e) {
                    console.log('End Handler Error:', e);
                }
            }
            throw error;
        }
    });
    return Worker;
})(wc.Worker);

exports['default'] = Worker;
module.exports = exports['default'];
//# sourceMappingURL=Worker.js.map
