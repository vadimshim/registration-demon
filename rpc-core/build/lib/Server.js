var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _InstanceBase2 = require('./InstanceBase');

var _InstanceBase3 = _interopRequireDefault(_InstanceBase2);

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

var Server = (function (_InstanceBase) {
    _inherits(Server, _InstanceBase);

    function Server(define, handlers) {
        _classCallCheck(this, Server);

        _InstanceBase.call(this, define, handlers);
    }

    Server.prototype.request = _asyncToGenerator(function* (client, _request) {
        if (!client) client = {};
        try {
            if (this.handlers.init) {
                yield this.handlers.init.call(client);
            }

            var method = this.getMethod(_request.method);
            var result = yield method(client, _request.params);

            if (this.handlers.end) {
                try {
                    yield this.handlers.end.call(client);
                } catch (e) {
                    console.log('End Handler Error:', e);
                }
            }

            return {
                client: client,
                respose: {
                    jsonrpc: "2.0",
                    id: _request.id,
                    result: result
                }
            };
        } catch (error) {
            if (error instanceof Error) {
                error = Errors.SystemError(error);
            }

            if (this.handlers.end) {
                try {
                    yield this.handlers.end.call(client);
                } catch (e) {
                    console.log('End Handler Error:', e);
                }
            }

            return {
                client: client,
                respose: {
                    jsonrpc: "2.0",
                    id: _request.id !== undefined ? _request.id : error.code === -32600 ? error.data : undefined,
                    error: error
                }
            };
        }
    });
    return Server;
})(_InstanceBase3['default']);

exports['default'] = Server;
module.exports = exports['default'];
//# sourceMappingURL=Server.js.map
