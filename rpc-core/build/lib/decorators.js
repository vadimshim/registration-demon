var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;
exports.internal = internal;
exports.module = _module;
exports.attr = attr;
exports.trigger = trigger;
exports.params = params;

var _Errors = require('./Errors');

var Error = _interopRequireWildcard(_Errors);

var _Param = require('./Param');

var _Method = require('./Method');

function internal(target, key, descriptor) {
    var method = _Method.createMethod(descriptor, key);
    method.internal = true;
}

function _module(name) {
    return function (target) {
        target.key = name;
    };
}

function attr(attr, value) {
    return function (target, key, descriptor) {
        var method = _Method.createMethod(descriptor, key);

        switch (attr.constructor) {
            case Object:
                for (var key in attr) {
                    if (!attr.hasOwnProperty(key)) continue;
                    method.attr[key] = attr[key];
                }
                break;

            case String:
                if (value !== undefined) {
                    method.attr[attr] = value;
                } else {
                    method.attr[attr] = true;
                }
                break;
        }
    };
}

function trigger(targetMethod, handler) {
    return function (target, key, descriptor) {
        var method = _Method.createMethod(descriptor, key);

        method.triggers.push({ targetMethod: targetMethod, handler: handler });
    };
}

function params(params) {
    return function (target, key, descriptor) {
        var method = _Method.createMethod(descriptor, key);

        for (var key in params) {
            if (!params.hasOwnProperty(key)) continue;
            method.params.push(_Param.Param.create(key, params[key]));
        }
    };
}
//# sourceMappingURL=decorators.js.map
