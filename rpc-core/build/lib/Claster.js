var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _os = require('os');

var _request2 = require('./request');

var _WorkerControl = require('./WorkerControl');

var _WorkerControl2 = _interopRequireDefault(_WorkerControl);

var _Errors = require('./Errors');

var Errors = _interopRequireWildcard(_Errors);

var Claster = (function () {
	function Claster(config) {
		_classCallCheck(this, Claster);

		this.init(config);
	}

	Claster.prototype.init = function init(config) {
		process.on('exit', this.abort.bind(this));

		if (!config.workersCount || config.workersCount <= 0) {
			config.workersCount = _os.cpus().length;
		}

		if (!config.workerFile) {
			//todo: Error
		}

		this.workerFile = config.workerFile;
		this.workersCount = config.workersCount;
		this.workers = [];

		for (var i = 0; i < this.workersCount; i++) {
			this.workers[i] = new _WorkerControl2['default'](this.workerFile);
		}
	};

	Claster.prototype.request = _asyncToGenerator(function* (client, _request) {
		try {
			var result = yield this.getWorker().sendTask({
				client: client,
				method: _request.method,
				params: _request.params
			});
			return {
				client: result.client,
				respose: {
					jsonrpc: "2.0",
					id: _request.id,
					result: result || undefined,
					error: result.error || undefined
				}
			};
		} catch (error) {
			if (error instanceof Error) {
				error = Errors.SystemError(error);
			}

			return {
				client: client,
				respose: {
					jsonrpc: "2.0",
					id: _request.id !== undefined ? _request.id : error.code === -32600 ? error.data : undefined,
					error: error
				}
			};
		}
	});

	Claster.prototype.restart = function restart() {
		var count = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];

		this.abort();
		this.workers = [];

		if (count !== 0) {
			this.workersCount = count;
		}

		for (var i = 0; i < this.workersCount; i++) {
			this.workers[i] = new _WorkerControl2['default'](this.workerFile);
		}
	};

	Claster.prototype.abort = function abort() {
		for (var i = 0; i < this.workersCount; i++) {
			this.workers[i].destroy();
		}
	};

	Claster.prototype.getWorker = function getWorker() {
		/*if( !worker ) {
  	throw Error.SystemError('Not ready workers');
  }*/
		var minWorker = this.workers[0];
		for (var i = 1; i < this.workersCount; i++) {
			var worker = this.workers[i];
			if (worker.count > minWorker.count) continue;
			minWorker = worker;
		}
		return minWorker;
	};

	return Claster;
})();

exports['default'] = Claster;
module.exports = exports['default'];
//# sourceMappingURL=Claster.js.map
