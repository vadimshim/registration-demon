var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

exports.__esModule = true;
exports.Error = Error;
exports.TransportError = TransportError;
exports.ParseError = ParseError;
exports.InvalidRequest = InvalidRequest;
exports.InvalidResponse = InvalidResponse;
exports.SystemError = SystemError;
exports.MethodNotFound = MethodNotFound;
exports.InvalidParams = InvalidParams;
exports.AccessDeniet = AccessDeniet;

var RPCError = function RPCError(message, code) {
    var data = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

    _classCallCheck(this, RPCError);

    this.message = message;
    this.code = code;
    this.data = data;
};

exports['default'] = RPCError;

function Error(message, code) {
    var data = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

    return new RPCError(message, code, data);
}

function TransportError() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Transport error.', -32300, data);
}

function ParseError() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Parse error.', -32700, data);
}

function InvalidRequest() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Invalid Request.', -32600, data);
}

function InvalidResponse() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Invalid Response.', -32600, data);
}

function SystemError() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('System error.', -32400, data ? {
        message: data.message,
        stack: data.stack
    } : null);
}

function MethodNotFound() {
    var method = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Method not found.', -32601, method);
}

function InvalidParams() {
    var param = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Invalid Params.', -32602, param);
}

function AccessDeniet() {
    var method = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new RPCError('Access deniet.', -32504, method);
}
//# sourceMappingURL=Errors.js.map
