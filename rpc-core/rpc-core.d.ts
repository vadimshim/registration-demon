declare module "rpc-core.d" {

    export interface RPCRequest {
        jsonrpc: string;
        id: number;
        method: string;
        params?: Object|Array;
    }

    export interface RPCResponse {
        jsonrpc: string;
        id?: number;
        result?: any;
        error?: {
            message: string;
            code: number;
            data?: any;
        };
    }

    export class Claster {
        constructor(options: {
            workersCount?: number;
            workerFile: string;
        });

        async request(client: any, request: {
            id: number;
            method: 'string';
            params?: Object;
        }): Promise<any>;

        restart(count?: number): void;
    }

    export class Worker {
        constructor(define: Function[], handlers: {
            /**
             * Метод вызывается при инициализации запроса.
             */
            init(): Promise<void>;

            /**
             * Метод вызывается при каждом вызове методов API
             * @param method Дескриптор метода
             * @param params Параметры вызова
             */
            call(method, params): Promise<void>;

            /**
             * Метод вызывается при если вызов метода API успешно завершен
             * @param method Дескриптор метода
             * @param params Параметры вызова
             * @param result Результат метода
             */
            done(method, params, result): Promise<void>;

            /**
             * Метод вызывается при если вызов метода API завершился ошибкой
             * @param method Дескриптор метода
             * @param params Параметры вызова
             * @param error Ошибка
             */
            fail(method, params, error): Promise<void>;
        });
    }

    export class Errors {
        static Error(message: string, code: number, data?: any);
        static TransportError(data?: any);
        static ParseError(data?: any);
        static InvalidRequest(data?: any);
        static SystemError(data?: any);
        static MethodNotFound(method?: string);
        static InvalidParams(param?: string);
        static AccessDeniet(method?: string);
    }

    interface Params {
        [name: string]: {
            type: Function;
            validate?: (value: any) => boolean | RegExp | any[];
            coerce?: (value: any) => any;
            convert?: (value: any) => any;
            default?: any;
        };
    }

    interface Attr {
        [name: string]: any;
    }

    export function parseResponse(response: string): RPCResponse;
    export function parseRequest(response: string): RPCRequest;

    export function stringifyResponse(response: RPCResponse): string;
    export function stringifyResponse(id: number, result: any, error: any): string;
    export function stringifyRequest(response: RPCRequest): string;
    export function stringifyRequest(id: number, method: string, params: Object|Array): string;


    export function module(name: string): (target: Function)=>void;
    export function internal(target: any, key: string, descriptor: any): void;
    export function params(params: Params): (target: any, key: string, descriptor: any)=>void;
    export function attr(attrs: Attr): (target: any, key: string, descriptor: any)=>void;
    export function attr(attr: string, value?: any): (target: any, key: string, descriptor: any)=>void;
}