var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _Map = require('babel-runtime/core-js/map')['default'];

var _Promise = require('babel-runtime/core-js/promise')['default'];

/**
 * Created by Vadim on 04.10.2016.
 */
var cp = require('child_process');
var eventEmitter = require('events');

var WorkerControler = (function (_eventEmitter) {
    _inherits(WorkerControler, _eventEmitter);

    function WorkerControler(file) {
        _classCallCheck(this, WorkerControler);

        _eventEmitter.call(this);
        this.file = file;
        this.init();
    }

    WorkerControler.prototype.init = function init() {
        var _this = this;

        this.isDestroy = false;
        this.count = 0;
        this.isReady = false;
        this.tasksArray = new _Map();
        this._GUID = 0;
        this.notices = new _Map();

        this.child = cp.fork(this.file);

        this.child.on('message', function (report) //подписка на события child
        {
            switch (report.type) {
                case 1:
                    //child готов к работе
                    _this.childReady();
                    break;

                case 11:
                    //child вернул результат
                    _this.childResponse(report);
                    break;

                case 12:
                    //child прислал уведомление
                    _this.noticeSub(report);
                    break;

                case 33:
                    //child выдал ошибку
                    break;
            }
        });

        this.child.on('close', this.childError.bind(this));
        this.child.on('exit', this.childError.bind(this));
        this.child.on('error', this.childError.bind(this));
    };

    WorkerControler.prototype.childReady = function childReady() {
        this.isReady = true;
        this.emit('ready', this);
    };

    WorkerControler.prototype.sendTask = function sendTask(task) {
        var _this2 = this;

        var id = this.GUID();
        return new _Promise(function (done, fail) {
            _this2.tasksArray.set(id, { done: done, fail: fail });
            _this2.child.send({
                type: 10,
                id: id,
                task: task
            });
            task = null;
            _this2.count++;
        });
    };

    WorkerControler.prototype.childResponse = function childResponse(report) {
        var response = this.tasksArray.get(report.id);
        if (response) {
            if (report.data.result) {
                response.done(report.data.result);
            } else if (report.data.error) {
                response.done(report.data.error);
            }

            this.tasksArray['delete'](report.id);
            this.count--;
        }

        if (this.count === 0) {
            this.tasksArray.clear();
            if (this.isDestroy) {
                this.killChild();
            }
        }
        this.child.send({ type: 100 });
    };

    WorkerControler.prototype.noticeSub = function noticeSub(notice) {
        if (this.notices.get(notice.channel)) {
            this.notices.get(notice.channel).push(notice.data);
        } else {
            this.notices.set(notice.channel, [notice.data]);
        }
    };

    WorkerControler.prototype.getNotice = function getNotice(channel) {
        if (this.notices.get(channel)) {
            return this.notices.get(channel);
        } else {
            return "Channel not found";
        }
    };

    WorkerControler.prototype.childError = function childError(e) {
        this.tasksArray.forEach(function (value) {
            var task = value;
            if (task) {
                task.fail(e || "Process Crushed");
            }
        });
        this.tasksArray.clear();
        this.count = 0;

        this.killChild();

        for (var i = 0; i < 20; i++) {
            if (!this.isReady && this.isDestroy) {
                if (this.isReady && !this.isDestroy) {
                    this.init();
                } else {
                    console.log("The process_child cannot be forked");
                }
            }
        }
    };

    WorkerControler.prototype.killChild = function killChild() {
        try {
            this.child.send({ type: 13 });
            this.child.removeAllListeners();
            this.child.kill('SIGHUP');
            this.child = null;
            this.init();
            this.emit('close', this);
        } catch (ex) {
            console.log(ex);
        }
    };

    WorkerControler.prototype.destroy = function destroy() {
        this.isDestroy = true;
        if (this.count === 0) {
            this.killChild();
        } else {
            return "This WorkerControl has " + this.count + " task(s) to do";
        }
    };

    WorkerControler.prototype.GUID = function GUID() {
        if (this._GUID >= 10000) this._GUID = 0;
        return this._GUID++;
    };

    return WorkerControler;
})(eventEmitter);

module.exports = WorkerControler;
//# sourceMappingURL=WorkerControler.js.map
