var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

/**
 * Created by Vadim on 04.10.2016.
 */

var Worker = (function () {
    function Worker() {
        _classCallCheck(this, Worker);

        this.initProcess();
    }

    Worker.prototype.initProcess = function initProcess() {
        var _this = this;

        process.on('message', function (task) {
            switch (task.type) {
                case 10:
                    _this.request(task.id, task.task);
                    break;
            }
        });

        setInterval(function () {
            if (!process.connected) {
                console.log("no connection with parent");
                process.abort();
            }
        }, 10000);

        process.send({ type: 1 });
    };

    Worker.prototype.request = _asyncToGenerator(function* (id, task) {
        try {
            if (this.onTask) {
                var result = yield this.onTask(task);
                if (result) sendResponse(undefined, result);
            }
        } catch (error) {
            sendResponse(error, undefined);
        }

        function sendResponse(error, result) {
            process.send({
                type: 11,
                id: id,
                data: {
                    error: error,
                    result: result
                }
            });
        }
    });

    Worker.prototype.noticePub = function noticePub(data, channel) {
        process.send({
            type: 12,
            channel: channel,
            data: data
        });
    };

    Worker.prototype.sendError = function sendError(e) {
        process.on('uncaughtException', function (e) {
            process.send({ type: 33, name: e.name, message: e.message, file: e.fileName });
        });
    };

    return Worker;
})();

module.exports = Worker;
//# sourceMappingURL=Worker.js.map
