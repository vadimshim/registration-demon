/**
 * Created by Vadim on 04.10.2016.
 */
const cp = require('child_process');
const eventEmitter = require('events');

class WorkerControler extends eventEmitter {
    constructor(file) {
        super();
        this.file = file;
        this.init();
    }

    init() {
        this.isDestroy = false;
        this.count = 0;
        this.isReady = false;
        this.tasksArray = new Map();
        this._GUID = 0;
        this.notices = new Map();

        this.child = cp.fork(this.file);

        this.child.on('message', (report) => //подписка на события child
        {
            switch ( report.type ) {
                case 1:     //child готов к работе
                    this.childReady();
                    break;

                case 11:   //child вернул результат
                    this.childResponse(report);
                    break;

                case 12:    //child прислал уведомление
                    this.noticeSub(report);
                    break;

                case 33:    //child выдал ошибку
                    break;
        }
    });

        this.child.on('close', this.childError.bind(this));
        this.child.on('exit', this.childError.bind(this));
        this.child.on('error', this.childError.bind(this));
    }

    childReady() {
        this.isReady = true;
        this.emit('ready', this);
    }

    sendTask(task) {
        var id = this.GUID();
        return new Promise((done, fail) => {
            this.tasksArray.set(id, {done, fail});
            this.child.send({
            type: 10,
            id: id,
            task: task
            });
            task = null;
            this.count++;
        })
    }

    childResponse(report) {
        var response = this.tasksArray.get(report.id);
        if( response ) {
            if(report.data.result){
                response.done(report.data.result);
            }else if(report.data.error){
                response.done(report.data.error);
            }

            this.tasksArray.delete(report.id);
            this.count--;
        }

        if( this.count === 0 ) {
            this.tasksArray.clear();
            if( this.isDestroy ) {
                this.killChild();
            }
        }
        this.child.send({type: 100});
    }

    noticeSub(notice){
        if(this.notices.get(notice.channel)){
            this.notices.get(notice.channel).push(notice.data);
        }else{
            this.notices.set(notice.channel, [notice.data]);
        }
    }

    getNotice(channel){
        if(this.notices.get(channel)){
            return this.notices.get(channel);
        }else{
            return "Channel not found";
        }
    }

    childError(e) {
        this.tasksArray.forEach(value =>{
            var task = value;
            if( task ) {
                task.fail(e || "Process Crushed");
            }
        })
        this.tasksArray.clear();
        this.count = 0;

        this.killChild();

        for(var i = 0; i < 20; i++){
            if(!this.isReady && this.isDestroy){
                if( this.isReady && !this.isDestroy ) {
                    this.init();
                }
                else {
                    console.log("The process_child cannot be forked");
                }
            }
        }
    }

    killChild()
    {
        try {
            this.child.send({type: 13});
            this.child.removeAllListeners();
            this.child.kill('SIGHUP');
            this.child = null;
            this.init();
            this.emit('close', this);
        }catch(ex){
            console.log(ex);
        }
    }

    destroy() {
        this.isDestroy = true;
        if( this.count === 0 ) {
            this.killChild();
        }else{
            return ("This WorkerControl has " + this.count + " task(s) to do");
        }
    }


    GUID() {
        if( this._GUID >= 10000 ) this._GUID = 0;
        return this._GUID++;
    }
}


module.exports = WorkerControler;