/**
 * Created by Vadim on 04.10.2016.
 */


class Worker {
    constructor() {
        this.initProcess();
    }

    initProcess()
    {
        process.on('message', task =>
        {
            switch ( task.type ) {
                case 10:
                    this.request(
                        task.id,
                        task.task
                    );
                break;
            }
        });

        setInterval(function()
        {
            if( !process.connected ) {
                console.log("no connection with parent");
                process.abort();
            }
        }, 10000);

        process.send({type: 1});
    }

    async request(id, task) {
        try {
            if(this.onTask) {
                var result = await this.onTask(task);
                if(result)
                sendResponse(undefined, result);
            }
        }catch(error) {
            sendResponse(error, undefined)
        }

        function sendResponse(error, result)
        {
          process.send({
              type: 11,
              id: id,
              data: {
                  error,
                  result
              }
          });
        }
    }

    noticePub(data, channel){
        process.send({
            type: 12,
            channel: channel,
            data: data
        })
    }

    sendError(e){
        process.on('uncaughtException', function(e){
            process.send({type: 33, name: e.name, message: e.message, file: e.fileName});
        })
    }
}



module.exports = Worker;