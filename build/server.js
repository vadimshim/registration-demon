var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _Promise = require('babel-runtime/core-js/promise')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _http = require('http');

var http = _interopRequireWildcard(_http);

var _rpcCore = require('rpc-core');

var RPC = _interopRequireWildcard(_rpcCore);

var _config = require('./config');

var config = _interopRequireWildcard(_config);

var Server = (function () {
    function Server(options, service) {
        _classCallCheck(this, Server);

        this.options = options;
        this.service = service;
        this.init();
    }

    Server.prototype.init = function init() {
        this.native = http.createServer(this.onRequest.bind(this));
        this.native.listen(this.options.port, this.options.host);
    };

    Server.prototype.onRequest = _asyncToGenerator(function* (request, response) {
        var requestTime = Date.now();

        try {
            var data = yield getData(request);
            var client = yield prepareClient(request);
            var result = yield this.service.request(client, data);

            sendResponse(result.client, RPC.stringifyResponse(result.respose));
        } catch (error) {
            sendResponse(client, RPC.stringifyResponse({
                error: error
            }));
        }

        function sendResponse(client, data) {
            data = new Buffer(data);
            var now = Date.now();

            var headers = {
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': data.length,
                'Server-Time': now,
                'Response-Time': now - requestTime
            };

            response.writeHead(200, headers);
            response.end(data);
        }
    });
    return Server;
})();

exports['default'] = Server;

function getData(request) {
    return new _Promise(function (done, fail) {
        request.once('data', function (data) {
            try {
                data = RPC.parseRequest(data);
                done(data);
            } catch (error) {
                fail(error);
            }
        });
    });
}

function prepareClient(request) {
    var client = {
        ip: request.headers['x-real-ip'],
        agent: request.headers['user-agent']
    };

    return client;
}
module.exports = exports['default'];
//# sourceMappingURL=server.js.map
