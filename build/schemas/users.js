/**
 * Created by Vadim on 13.10.2016.
 */

var mongoose = require('mongoose');
var db = require('../connection');

var UserShema = new mongoose.Schema({
    name: { type: String, required: false, unique: true },
    surname: { type: String, required: false, unique: true },
    email: { type: String, required: true, unique: true },
    checked: { type: Boolean, required: false }
});

var User = db.model("Users", UserShema);

module.exports = User;
//# sourceMappingURL=users.js.map
