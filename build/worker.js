var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _sourceMapSupport = require('source-map-support');

var sourceMap = _interopRequireWildcard(_sourceMapSupport);

var _rpcCore = require('rpc-core');

var rpc = _interopRequireWildcard(_rpcCore);

var _config = require('./config');

var config = _interopRequireWildcard(_config);

var _modulesUsers = require('./modules/users');

var _modulesUsers2 = _interopRequireDefault(_modulesUsers);

sourceMap.install();

global.worker = new rpc.Worker([_modulesUsers2['default']], {
    init: _asyncToGenerator(function* () {
        console.log('init request');
    }),

    end: _asyncToGenerator(function* () {
        console.log('end request');
    }),

    call: _asyncToGenerator(function* (method, params) {
        console.log('call method:', method.fullName);
    }),

    done: _asyncToGenerator(function* (method, params, result) {
        console.log('done method:', method.fullName);
    }),

    fail: _asyncToGenerator(function* (method, params, error) {
        console.log('fail method:', method.fullName);

        if (error instanceof Error) {
            error.stack = sourceMap.getErrorSource(error);
            if (error.stack) {
                error.stack = error.stack.substr(1);
            }

            console.log(error.message);
            console.log(error.stack);
        }
    })
});
//# sourceMappingURL=worker.js.map
