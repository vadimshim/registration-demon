var _createDecoratedClass = require('babel-runtime/helpers/create-decorated-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

exports.__esModule = true;

var _rpcCore = require('rpc-core');

var RPC = _interopRequireWildcard(_rpcCore);

var User = require("../schemas/users");

var utils = {
    trim: function trim(value) {
        if (value === null) {
            return null;
        }
        return value.trim();
    }
};

var Users = (function () {
    function Users() {
        _classCallCheck(this, _Users);
    }

    _createDecoratedClass(Users, null, [{
        key: 'registration',
        decorators: [RPC.params({
            name: { type: String, coerce: function coerce(value) {
                    value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||@|\"|\+|\=|^(\b)/ig, function () {
                        return "";
                    });
                    value = value.replace(/\s+/g, function () {
                        return " ";
                    });
                    value = utils.trim(value);
                    return value;
                } },
            surname: { type: String, coerce: function coerce(value) {
                    value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||@|\"|\+|\=|^(\b)/ig, function () {
                        return "";
                    });
                    value = value.replace(/\s+/g, function () {
                        return " ";
                    });
                    value = utils.trim(value);
                    return value;
                } },
            email: { type: String, coerce: function coerce(value) {
                    value = value.replace(/<[\S\s]*>[\S\s]*<?\/([\S\s]*>)?|{|}|<|>|\&|;|\#|\*|\^|\/|%|\?|\\|\(|\)|\№|\||\"|\+|\=|^(\b)/ig, function () {
                        return "";
                    });
                    value = value.replace(/\s+/g, function () {
                        return " ";
                    });
                    value = utils.trim(value);
                    return value;
                } }
        }), RPC.attr('session', 'not')],
        value: _asyncToGenerator(function* (params) {
            try {
                var user = yield new User({ name: params.name, surname: params.surname, email: params.email, checked: false });
                yield user.save();
                var users = yield User.find();
                return { code: -33000, message: "User successfully added" };
            } catch (ex) {
                console.log(ex);
                if (ex.name == "MongoError" && ex.code == 11000) {
                    // проверка на дубликаты полей name, surname, email
                    var regName = new RegExp(params.name);
                    var regSurname = new RegExp(params.surname);
                    var regEmail = new RegExp(params.email);
                    if (regName.test(ex.message)) {
                        throw Errors.DublicateName(params.name);
                    } else if (regSurname.test(ex.message)) {
                        throw Errors.DublicateSurname(params.surname);
                    } else if (regEmail.test(ex.message)) {
                        throw Errors.DublicateEmail(params.email);
                    }
                }
                if (ex.name == "ValidationError") {
                    //Проверка на пустой адрес почты
                    throw Errors.EmptyEmail(ex.email);
                }
                throw Errors.UserCreateError(ex);
            }
        })
    }]);

    var _Users = Users;
    Users = RPC.module('users')(Users) || Users;
    return Users;
})();

exports['default'] = Users;

var Errors = (function () {
    function Errors() {
        _classCallCheck(this, Errors);
    }

    Errors.DublicateName = function DublicateName(data) {
        return RPC.Errors.Error('Dublicate name', -31000, data);
    };

    Errors.DublicateSurname = function DublicateSurname(data) {
        return RPC.Errors.Error('Dublicate surname', -31001, data);
    };

    Errors.DublicateEmail = function DublicateEmail(data) {
        return RPC.Errors.Error('Dublicate email', -31002, data);
    };

    Errors.UserCreateError = function UserCreateError(data) {
        return RPC.Errors.Error('User create error', -31099, data);
    };

    Errors.EmptyEmail = function EmptyEmail(data) {
        return RPC.Errors.Error('Email is required', -31010, data);
    };

    return Errors;
})();

module.exports = exports['default'];
//# sourceMappingURL=users.js.map
