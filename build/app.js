var _interopRequireWildcard = require('babel-runtime/helpers/interop-require-wildcard')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _rpcCore = require('rpc-core');

var RPC = _interopRequireWildcard(_rpcCore);

var _server = require('./server');

var _server2 = _interopRequireDefault(_server);

var _config = require('./config');

var config = _interopRequireWildcard(_config);

require('source-map-support').install();

var service = new RPC.Claster(config.claster);
var server = new _server2['default'](config.server, service);
var db = require("./connection");

console.log("Server started on port:", config.server.port, ", host:", config.server.host);
//# sourceMappingURL=app.js.map
