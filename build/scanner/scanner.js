var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

/**
 * Created by Vadim on 13.10.2016.
 */

var User = require("../schemas/users");
var transporter = require('./mailer');
var messages = require('./message');
var fs = require('fs');
var ObjectId = require('objectid');
var json2csv = require('json2csv');

var Scanner = (function () {
    function Scanner() {
        _classCallCheck(this, Scanner);
    }

    Scanner.prototype.scan = _asyncToGenerator(function* () {
        setInterval(_asyncToGenerator(function* () {
            try {
                var users = yield User.find({ checked: false }); //get all new (unchecked) users

                if (users.length > 0) {

                    //Date of email send
                    var date = new Date();
                    var month = date.getMonth();
                    var day = date.getDate();
                    var hour = date.getHours();
                    var minute = date.getMinutes();
                    var filename = month + 1 + "." + day + "(" + hour + "-" + minute + ").csv";

                    //Write users to csv file
                    json2csv({ data: users, fields: ['name', 'surname', 'email'] }, function (err, csv) {
                        if (err) console.log(err);
                        fs.writeFile(filename, csv, function (err) {
                            if (err) throw err;
                            console.log('file saved');
                        });
                    });
                    //end write csv

                    messages["attachments"] = [{ path: "./" + filename }]; //attaching csv file to email
                    transporter.sendMail(messages); //sending email
                    var allChecked = yield User.update({ checked: false }, { $set: { checked: true } }); //marking all users as checked
                } else {
                        //console.log("No new users found");
                    }
            } catch (ex) {
                console.log(ex);
            }
        }), 28800); //28800 milliseconds - send email three times a day
    });
    return Scanner;
})();

var scanner = new Scanner();
scanner.scan();
//# sourceMappingURL=scanner.js.map
